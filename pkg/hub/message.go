// Copyright © 2021 Ettore Di Giacinto <mudler@mocaccino.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, see <http://www.gnu.org/licenses/>.

package hub

import (
	"github.com/mudler/edgevpn/pkg/utils"
	"github.com/pkg/errors"
)

// Message gets converted to/from JSON and sent in the body of pubsub messages.
type Message struct {
	Message  string
	SenderID string

	Annotations map[string]string
}

type MessageOption func(cfg *Message) error

// Apply applies the given options to the config, returning the first error
// encountered (if any).
func (m *Message) Apply(opts ...MessageOption) error {
	for _, opt := range opts {
		if opt == nil {
			continue
		}
		if err := opt(m); err != nil {
			return err
		}
	}
	return nil
}

func NewMessage(s string) *Message {
	return &Message{Message: s}
}

func (m *Message) Seal(key string) error {
	enckey := [32]byte{}
	copy(enckey[:], key)
	enc, err := utils.AESEncrypt(m.Message, &enckey)
	if err != nil {
		return errors.Wrap(err, "while sealing message")
	}
	m.Message = enc
	return nil
}

func (m *Message) Unseal(key string) error {
	enckey := [32]byte{}
	copy(enckey[:], key)
	dec, err := utils.AESDecrypt(m.Message, &enckey)
	if err != nil {
		return errors.Wrapf(err, "while unsealing message from peer: %s", m.SenderID)
	}
	m.Message = dec
	return nil
}

func (m *Message) Copy() *Message {
	copy := *m
	return &copy
}

func (m *Message) WithMessage(s string) *Message {
	copy := m.Copy()
	copy.Message = s
	return copy
}
